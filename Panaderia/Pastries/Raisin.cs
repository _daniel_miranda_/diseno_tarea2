namespace Pastries
{
    class Raisin : SweetBread
    {
        public Raisin()
        {
            this.cantidadharina_ = "400 g";
            this.cantidadazucar_ = "30 g";
            this.cantidadagua_ = "20 ml";
            this.cantidadlevadura_ = "7 g";
            this.cantidadsal_ = "1 cucharada";
        }


        public override string ToString()
        {
            string Recipe = $"Tipo de pan: {this.Type_}, Pan con pasas \n Ingredientes: \n    -Cantidad de harina: {this.cantidadharina_} \n    -Cantidad de azucar: {this.cantidadazucar_} \n    -Cantidad de agua: {this.cantidadagua_} \n    -Cantidad de levadura: {this.cantidadlevadura_} \n    -Cantidad de sal: {this.cantidadsal_}";
            Console.WriteLine(Recipe);
            return Recipe;
        }
    }
}
namespace Pastries
{
    class Chicken : SaltyBread
    {
        public Chicken()
        {
            this.cantidadharina_ = "200 g";
            this.cantidadazucar_ = "10 g";
            this.cantidadagua_ = "30 ml";
            this.cantidadlevadura_ = "3 g";
            this.cantidadsal_ = "2 cucharadas";
        }


        public override string ToString()
        {
            string Recipe = $"Tipo de pan: {this.Type_}, pan salado relleno de pollo \n Ingredientes: \n    -Cantidad de harina: {this.cantidadharina_} \n    -Cantidad de azucar: {this.cantidadazucar_} \n    -Cantidad de agua: {this.cantidadagua_} \n    -Cantidad de levadura: {this.cantidadlevadura_} \n    -Cantidad de sal: {this.cantidadsal_}";
            Console.WriteLine(Recipe);
            return Recipe;
        }
    }
}
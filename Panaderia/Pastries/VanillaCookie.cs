namespace Pastries
{
    class Vanilla : Cookie
    {
        public Vanilla()
        {
            this.cantidadharina_ = "55 g";
            this.cantidadazucar_ = "15 g";
            this.cantidadagua_ = "30 ml";
            this.cantidadlevadura_ = "3 g";
            this.cantidadsal_ = "1 cucharadas";
        }


        public override string ToString()
        {
            string Recipe = $"Tipo de pan: {this.Type_}, Galleta de Vainilla \n Ingredientes: \n    -Cantidad de harina: {this.cantidadharina_} \n    -Cantidad de azucar: {this.cantidadazucar_} \n    -Cantidad de agua: {this.cantidadagua_} \n    -Cantidad de levadura: {this.cantidadlevadura_} \n    -Cantidad de sal: {this.cantidadsal_}";
            Console.WriteLine(Recipe);
            return Recipe;
        }
    }
}
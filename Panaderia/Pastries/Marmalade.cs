namespace Pastries
{
    class Marmalade : SweetBread
    {
        public Marmalade()
        {
            this.cantidadharina_ = "500 g";
            this.cantidadazucar_ = "50 g";
            this.cantidadagua_ = "280 ml";
            this.cantidadlevadura_ = "25 g";
            this.cantidadsal_ = "1 cucharada";
        }


        public override string ToString()
        {
            string Recipe = $"Tipo de pan: {this.Type_}, Pan con Mermelada \n Ingredientes: \n    -Cantidad de harina: {this.cantidadharina_} \n    -Cantidad de azucar: {this.cantidadazucar_} \n    -Cantidad de agua: {this.cantidadagua_} \n    -Cantidad de levadura: {this.cantidadlevadura_} \n    -Cantidad de sal: {this.cantidadsal_}";
            Console.WriteLine(Recipe);
            return Recipe;
        }
    }
}
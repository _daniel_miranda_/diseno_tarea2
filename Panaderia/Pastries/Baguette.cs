namespace Pastries
{
    class Baguette : SaltyBread
    {
        public Baguette()
        {
            this.cantidadharina_ = "600 g";
            this.cantidadazucar_ = "10 g";
            this.cantidadagua_ = "100 ml";
            this.cantidadlevadura_ = "10 g";
            this.cantidadsal_ = "3 cucharadas";
        }


        public override string ToString()
        {
            string Recipe = $"Tipo de pan: {this.Type_}, Baguette \n Ingredientes: \n    -Cantidad de harina: {this.cantidadharina_} \n    -Cantidad de azucar: {this.cantidadazucar_} \n    -Cantidad de agua: {this.cantidadagua_} \n    -Cantidad de levadura: {this.cantidadlevadura_} \n    -Cantidad de sal: {this.cantidadsal_}";
            Console.WriteLine(Recipe);
            return Recipe;
        }
    }
}